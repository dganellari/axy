
#include "axy.hpp"

int main(int argc, char *argv[]) {

  Kokkos::initialize(argc, argv);
  {
    axy::axpy();
  }

  Kokkos::finalize();

  return 0;
}
