cmake_minimum_required(VERSION 2.8.12)

project(axy)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_CURRENT_SOURCE_DIR}/cmake")

if (NOT TRILINOS_DIR)
	message(STATUS "Setting TRILINOS_DIR to $ENV{TRILINOS_DIR}")
	set(TRILINOS_DIR $ENV{TRILINOS_DIR} CACHE PATH "Directory where Kokkos is installed")
endif()

if (NOT KOKKOS_DIR)
	message(STATUS "Setting KOKKOS_DIR to $ENV{KOKKOS_DIR}")
	set(KOKKOS_DIR $ENV{KOKKOS_DIR} CACHE PATH "Directory where Kokkos is installed")
endif()

FIND_PACKAGE(Kokkos HINTS ${KOKKOS_DIR}/lib/CMake/Kokkos ${TRILINOS_DIR}/lib/cmake/Kokkos QUIET)

IF(Kokkos_FOUND)
	MESSAGE("\nFound Kokkos!  Here are the details: ")
	MESSAGE("   Kokkos_CXX_COMPILER = ${Kokkos_CXX_COMPILER}")
	MESSAGE("   Kokkos_C_COMPILER = ${Kokkos_C_COMPILER}")
	
	IF(Kokkos_CXX_COMPILER)
		SET(CMAKE_C_COMPILER ${Kokkos_C_COMPILER})
		SET(CMAKE_CXX_COMPILER ${Kokkos_CXX_COMPILER})
		MESSAGE("Setting CMAKE_CXX_COMPILER to Kokkos_CXX_COMPILER")
	ENDIF()
	
	set(WITH_KOKKOS ON)
	
	FIND_PACKAGE(OpenMP)

	IF(OPENMP_FOUND)
		SET (CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS} ${OpenMP_Fortran_FLAGS}")
		SET (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
		SET (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
		set (CMAKE_CXX_LINK_FLAGS "${CMAKE_CXX_LINK_FLAGS} ${OpenMP_CXX_FLAGS}")
	ENDIF()
	
ELSE()
	MESSAGE(WARNING "Could not find Kokkos!")
ENDIF()

	
IF(Kokkos_FOUND)
	list(APPEND HEADERS
	    utils.hpp
		axy.hpp
	)
	list(APPEND SOURCES
		axy.cpp
	)
ENDIF()

set(CMAKE_CXX_DEBUG "-g")

add_library(axy STATIC ${HEADERS} ${SOURCES})

add_definitions("-std=c++11")
target_include_directories(axy PUBLIC .)

IF(Kokkos_FOUND)
	MESSAGE(" Kokkos_INCLUDE_DIRS = ${Kokkos_INCLUDE_DIRS}")
	MESSAGE(" Kokkos_LIBRARIES = ${Kokkos_LIBRARIES}")
	MESSAGE(" Kokkos_TPL_LIBRARIES = ${Kokkos_TPL_LIBRARIES}")
	MESSAGE(" Kokkos_LIBRARY_DIRS = ${Kokkos_LIBRARY_DIRS}")
	
	target_include_directories(axy PUBLIC ${Kokkos_TPL_INCLUDE_DIRS} ${Kokkos_INCLUDE_DIRS})

	message(STATUS "Kokkos_INCLUDE_DIRS=${Kokkos_INCLUDE_DIRS}, ${Kokkos_TPL_INCLUDE_DIRS}")
	
	IF(Kokkos_CXX_COMPILER)
		target_link_libraries(axy ${Kokkos_LIBRARIES} ${Kokkos_TPL_LIBRARIES})
	ELSE()
		target_link_libraries(axy ${Kokkos_LIBRARIES} ${Kokkos_TPL_LIBRARIES} -L${Kokkos_LIBRARY_DIRS})
	ENDIF()
ENDIF()

add_executable(axy_exec main.cpp)
target_link_libraries(axy_exec axy)

target_link_libraries(axy ${LIBRARIES})
target_include_directories(axy PUBLIC ${CMAKE_BINARY_DIR})
target_include_directories(axy PUBLIC ${INCLUDES})
