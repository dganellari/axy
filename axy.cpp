#include "axy.hpp"

namespace axy {

void axpy() {

  int N = 4096 * 10;
  int M = 1024 * 10;

  ViewVectorType<int> x("x", M);
  ViewVectorType<int> y("x", N);
  ViewMatrixType<int> A("A", N, M);

  ViewVectorType<int>::HostMirror h_x = Kokkos::create_mirror_view(x);
  ViewVectorType<int>::HostMirror h_y = Kokkos::create_mirror_view(y);
  ViewMatrixType<int>::HostMirror h_A = Kokkos::create_mirror_view(A);

  // Initialize x vector on host.
  for (int i = 0; i < M; ++i) {
    h_x(i) = 1;
  }

  // Initialize A matrix on host.
  for (int i = 0; i < N; ++i) {
    for (int j = 0; j < M; ++j) {
      h_A(i, j) = 1;
    }
  }

  // Deep copy host views to device views.
  Kokkos::deep_copy(x, h_x);
  Kokkos::deep_copy(A, h_A);

  Kokkos::Timer timer;

  Kokkos::parallel_for("Ax=Y", N, KOKKOS_LAMBDA(int i) {
        double temp2 = 0;

        for (int j = 0; j < M; ++j) {
          temp2 += A(i, j) * x(j);
        }

        y(i) = temp2;
      });

  // Calculate time.
  double time = timer.seconds();
  printf("Took %g seconds to perform Ax=y\n", time);

  Kokkos::deep_copy(h_y, y);
}
} // namespace axy