#ifndef GENERATION_UTILS_KOKKOS_HPP_
#define GENERATION_UTILS_KOKKOS_HPP_

#include <Kokkos_Core.hpp>
#include <Kokkos_UnorderedMap.hpp>

namespace axy {

#ifdef KOKKOS_ENABLE_CUDA
	#define KokkosMemSpace Kokkos::CudaSpace
	#define KokkosLayout Kokkos::LayoutLeft
#else // USE_CUDA
	#ifdef KOKKOS_ENABLE_OPENMP
		#define KokkosMemSpace Kokkos::OpenMP
		#define KokkosLayout Kokkos::LayoutRight
	#else // KOKKOS_ENABLE_OPENMP
		#define KokkosMemSpace Kokkos::Serial
		#define KokkosLayout Kokkos::LayoutRight
	#endif // KOKKOS_ENABLE_OPENMP
#endif // USE_CUDA

template <typename T>
using ViewVectorType = Kokkos::View<T *, KokkosLayout, KokkosMemSpace>;

template <typename T>
using ViewMatrixType = Kokkos::View<T **, KokkosLayout, KokkosMemSpace>;

template <typename Key, typename Value>
using UnorderedMap = Kokkos::UnorderedMap<Key, Value, KokkosMemSpace>;

} // namespace axy
#endif
